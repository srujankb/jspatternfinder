PF = {
	consts : {
		ANY : "any",
		STRING : "string",
		NUMBER : "number",
		BOOLEAN : "boolean",
		UNDEFINED : "undefined",
		FUNCTION : "function",
		HAS_OBJECT : "hasObject",
		HAS_OBJECTS : "hasObjects",
		HAS_ARRAY : "hasArray",
		FOR_EACH : "forEach"
	},
	helpers : {
		'isDefined' : function( value){
			if(typeof(value) == "undefined" || value === null)
				return false;
			else
				return true;
		},
		'checkValidKey' : function( pattern_key, data_key){
			if( !PF.helpers.isDefined(pattern_key) || pattern_key === PF.consts.ANY || pattern_key === data_key)
				return true;
			else
				return false;
		}
	}
};

PatternFinder = function(topLevelObject){
	this._topLevelObject = topLevelObject;
};

PatternFinder.prototype.hasObject = function( pattern, Obj){
	if(!PF.helpers.isDefined(Obj))
		Obj = this._topLevelObject;

};

PatternFinder.prototype.hasObjects = function( pattern, Obj){
	if(!PF.helpers.isDefined(Obj))
		Obj = this._topLevelObject;
	for( var item in pattern)
		this.processPattern(pattern[item], Obj);
};

PatternFinder.prototype.hasArray = function( pattern, Obj){

};

PatternFinder.prototype.processPattern = function( pattern, Obj){
	if(PF.helpers.isDefined(pattern.type)){
		switch(pattern.type)
		{
			case PF.consts.HAS_OBJECTS:
				this.hasObjects( pattern, Obj);
				break;
			case PF.consts.HAS_OBJECT:
				this.hasObject( pattern, Obj);
				break;
			case PF.consts.HAS_ARRAY:
				this.hasArray( pattern, Obj);
				break;
			case PF.consts.FOR_EACH:
				this.forEach( pattern, Obj);
				break;
		}
	}
};

PatternFinder.prototype.forEach = function( pattern, Obj){
	if(!PF.helpers.isDefined(Obj))
		Obj = this._topLevelObject;
	if(PF.helpers.isDefined(pattern.val)){
		for( var key in Obj){
			if(PF.helpers.checkValidKey( pattern[key], key))
				this.processPattern( pattern.val, Obj[key]);
		}
	}
};